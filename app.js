var express = require('express')
var path = require('path')
var app = express()
var data = require('./Assets/data.json')
app.use(express.static(path.join(__dirname,'Assets')))
app.get('/',(req,res) => {
res.setHeader('Content-Type', 'application/json');
  res.json(data) 
})
app.get('/:id', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params.id, 10) 
    res.json(data[id])
})
app.listen(8000,() => {
    console.log("listening on port 8000")
})